/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unlar.paradigmas3.parcial2.roldansabattini.objetos;

/**
 *
 * @author Nahuel Sabattini
 */
public class Profesor extends Persona {
    
    private String legajo; 

    public Profesor(String legajo) {
        this.legajo = legajo;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    @Override
    public String toString() {
        return "Profesor{" + "legajo=" + legajo + '}';
    }

    public Profesor(String documento, String apellido, String nombre) {
        super(documento, apellido, nombre);
    }

    public Profesor() {
    }
    
    
    
    
    
}
