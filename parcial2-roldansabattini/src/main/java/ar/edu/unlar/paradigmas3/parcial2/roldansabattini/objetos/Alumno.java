/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unlar.paradigmas3.parcial2.roldansabattini.objetos;

/**
 *
 * @author Nahuel Sabattini
 */
public class Alumno extends Persona {
    
    private String matricula;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return "Alumno{" + "matricula=" + matricula + '}';
    }
    
    

    public Alumno(String matricula) {
        this.matricula = matricula;
    }

    
    public Alumno(String documento, String apellido, String nombre) {
        super(documento, apellido, nombre);
    }

    public Alumno() {
    }
    
    
    
    
    
    
    
    
}
