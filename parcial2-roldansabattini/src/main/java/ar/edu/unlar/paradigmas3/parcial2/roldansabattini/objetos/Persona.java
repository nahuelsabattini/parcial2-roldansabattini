/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unlar.paradigmas3.parcial2.roldansabattini.objetos;

/**
 *
 * @author Nahuel Sabattini
 */
public class Persona {
    
    private String documento;
    private String apellido;
    private String nombre; 

    public Persona(String documento, String apellido, String nombre) {
        this.documento = documento;
        this.apellido = apellido;
        this.nombre = nombre;
    }

    public Persona() {
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona{" + "documento=" + documento + ", apellido=" + apellido + ", nombre=" + nombre + '}';
    }
    
    
    
    
    
}
