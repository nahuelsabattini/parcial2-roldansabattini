/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unlar.paradigmas3.parcial2.roldansabattini.objetos;

/**
 *
 * @author Nahuel Sabattini
 */
public class Aula {
    
    private int aula;
    private int modulo; 

    public Aula() {
    }

    public Aula(int aula, int modulo) {
        this.aula = aula;
        this.modulo = modulo;
    }

    public int getAula() {
        return aula;
    }

    public void setAula(int aula) {
        this.aula = aula;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    @Override
    public String toString() {
        return "Aula{" + "aula=" + aula + ", modulo=" + modulo + '}';
    }
    
    
    
    
}
