/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unlar.paradigmas3.parcial2.roldansabattini.objetos;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Nahuel Sabattini
 */
public class Curso {
    
    
    Aula aula1 = new Aula();
    Profesor profesor1 = new Profesor();
    
    
    private ArrayList<Alumno>alumnos; //composición 
    
    //Delegate

    public int sizeAlumnos() {
        return alumnos.size();
    }

    public boolean addAlumnos(Alumno e) {
        return alumnos.add(e);
    }

    public boolean removeAlumnos(Object o) {
        return alumnos.remove(o);
    }

    public void clearAlumnos() {
        alumnos.clear();
    }
    
    
    
    
    
    
    private Date fecha; 
    private String catedra; 

    public Curso() {
    }

    public Curso(Date fecha, String catedra) {
        this.fecha = fecha;
        this.catedra = catedra;
    }
    
    

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getCatedra() {
        return catedra;
    }

    public void setCatedra(String catedra) {
        this.catedra = catedra;
    }

    @Override
    public String toString() {
        return "Curso{" + "fecha=" + fecha + ", catedra=" + catedra + '}';
    }
    
    
    
}
